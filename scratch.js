const paragraphs = document.querySelectorAll('p');
paragraphs.forEach((paragraph) => {
    paragraph.style.backgroundColor = '#ff0000';
});

const testParagraph = document.querySelector('.testParagraph');
testParagraph.textContent = 'This is a paragraph';

const mainHeaderElements = document.querySelector('.main-header').children;
console.log(mainHeaderElements);

const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach((title) => {
    title.classList.remove('section-title');
});
